using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Storage
{
    public class ItemContainer : MonoBehaviour
    {
        public event Action ItemCollectionUpdated;
        
        public Transform[] places;

        private Item[] _items;

        public int AvailablePlaces => places.Length - _items.Count(p => p != null);

        public IEnumerable<Item> Items => _items.Where(i => i != null); 

        public int Capacity => places.Length;

        private void Start()
        {
            _items = new Item[places.Length];
        }

        public bool Contains(int itemID, int amount = 1)
            => _items.Count(i => i != null && i.itemID == itemID) >= amount;

        public bool TryPlaceItem(Item item)
        {
            if (AvailablePlaces == 0)
                return false;

            var availablePlaceIndex = Array.FindIndex(_items, i => i == null);

            if (this == null)
                return false;
            
            item.MoveTo(places[availablePlaceIndex]);
            _items[availablePlaceIndex] = item;
            
            ItemCollectionUpdated?.Invoke();
            return true;
        }

        public bool TryTakeItem(int itemID, out Item item)
        {
            var index = Array.FindIndex(_items, i => i != null && i.itemID == itemID);
            
            if (index == -1)
            {
                item = null;
                return false;
            }
            
            item = _items[index];
            
            _items[index] = null;
            
            ItemCollectionUpdated?.Invoke();
            return true;
        }

        private void OnDrawGizmosSelected()
        {
            foreach (var place in places.Select(p => p.position))
            {
                Gizmos.DrawSphere(place, 0.1f);
            }
        }
    }
}
