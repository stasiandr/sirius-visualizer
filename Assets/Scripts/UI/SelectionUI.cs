using System;
using Agents;
using Player;
using Storage;
using TMPro;
using UnityEngine;

namespace UI
{
    public class SelectionUI : MonoBehaviour
    {
        [SerializeField]
        private GameObject window;
        
        [SerializeField]
        private TextMeshProUGUI header;
        
        [SerializeField]
        private MapDisplay mapDisplay;

        [SerializeField]
        private AgentDisplay agentDisplay;

        [SerializeField]
        private ItemContainerDisplay itemContainerDisplay;


        private float timer;
        private void Update()
        {
            timer += Time.deltaTime;
        
            if (Input.GetMouseButtonDown(0))
                timer = 0;
            if (Input.GetMouseButtonUp(0) && timer < 0.5f)
                HandleClick();
        }

        private void HandleClick()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (!Physics.Raycast(ray, out var hit, 1000)) return;

            DisableAllDisplays();

            window.SetActive(true);
            switch (hit.collider.tag)
            {
                case "Floor":
                    mapDisplay.Coords = new Vector2Int((int) hit.point.x, (int) hit.point.z);
                    header.text = "Map";
                    mapDisplay.gameObject.SetActive(true);
                    return;
                case "Agent":
                    agentDisplay.MyAgent = hit.collider.GetComponentInParent<Agent>();
                    PlayerController.FollowTransform = hit.collider.transform;
                    header.text = "Agent";
                    agentDisplay.gameObject.SetActive(true);
                    return;
                case "Shelf":
                    itemContainerDisplay.Container = hit.collider.GetComponentInParent<ItemContainer>();
                    header.text = "Shelf";
                    itemContainerDisplay.gameObject.SetActive(true);
                    return;
            }
        }

        public void DisableAllDisplays()
        {
            agentDisplay.MyAgent = null;
            mapDisplay.gameObject.SetActive(false);
            agentDisplay.gameObject.SetActive(false);
            itemContainerDisplay.gameObject.SetActive(false);
            PlayerController.UnfollowTarget();
        }
    }
}