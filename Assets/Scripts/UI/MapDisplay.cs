using TMPro;
using UnityEngine;

namespace UI
{
    public class MapDisplay : MonoBehaviour
    {
        public TextMeshProUGUI coordsDisplay;
        
        public Vector2Int Coords
        {
            set => coordsDisplay.text = value.ToString();
        }
    }
}