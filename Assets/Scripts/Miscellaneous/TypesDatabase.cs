using System;
using System.Collections.Generic;
using UnityEngine;

namespace Miscellaneous
{
    public class TypesDatabase<TKey, TValue> : ScriptableObject
    {
        [Serializable]
        public struct Pair
        {
            public TKey key;
            public TValue value;
        }

        private static TypesDatabase<TKey, TValue> _instance;

        private static string PathToAsset
        {
            get
            {
                Type declaringType = typeof(TKey).DeclaringType;
                return declaringType != null ? declaringType.Name : typeof(TKey).Name;
            }
        }

        public static TypesDatabase<TKey, TValue> Instance
        {
            get
            {
                if (_instance == null)
                    _instance = Resources.Load<TypesDatabase<TKey, TValue>>(PathToAsset);

                return _instance;
            }
        }


        [SerializeField] private Pair[] dataBase;

        public TValue this[TKey key] => Instance.GetEntityByName(key);

        private TValue GetEntityByName(TKey key)
        {
            for (int i = 0; i < dataBase.Length; i++)
                if (dataBase[i].key.Equals(key))
                    return dataBase[i].value;

            throw new KeyNotFoundException($"{PathToAsset} \"{key}\" not found");
        }
    }
}