using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Miscellaneous
{
    public class AddressableLoader : MonoBehaviour
    {
        public event Action ResourcesLoaded;
        
        public List<AssetReference> references;
        
        public bool Initialized { get; private set; }
        
       private AsyncOperationHandle<IList<GameObject>> _handle;
        
        private IEnumerator Start()
        {
            yield return LoadResources(references);
        }

        private void OnDestroy()
        {
            Addressables.Release(_handle);
        }


        public IEnumerator LoadResources(List<AssetReference> assetReferences)
        {
            var locationsHandle = Addressables.LoadResourceLocationsAsync(assetReferences);

            yield return locationsHandle;
            
            _handle = Addressables.LoadAssetsAsync<GameObject>(locationsHandle.Result, Debug.Log);

            yield return _handle;
            Initialized = true;
            
            ResourcesLoaded?.Invoke();
        }
    }
}