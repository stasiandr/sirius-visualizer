using System;
using System.Collections;
using System.Collections.Generic;
using Misc;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Miscellaneous
{
    public class ItemsLoader : MonoBehaviour
    {
        private const string UniqueItemsResourceLocation = "uniqueItem";
        private const string ColorCodedItemsResourceLocation = "colorCodedItem";

        private static List<Mesh> _uniqueItemMeshes;
        private static List<Mesh> _colorCodedItemMeshes;

        private static bool _initializationStarted;

        public static bool Initialized { get; private set; }

        private static IEnumerator Load(string uniqueItemsResourceLocation, ICollection<Mesh> uniqueItemMeshes)
        {
            var handle = Addressables.LoadResourceLocationsAsync(uniqueItemsResourceLocation);
            yield return handle;

            uniqueItemMeshes.Clear();

            foreach (var location in handle.Result)
            {
                if (location.ResourceType != typeof(GameObject)) continue;

                var assetHandle = Addressables.LoadAssetAsync<GameObject>(location);
                yield return assetHandle;
                uniqueItemMeshes.Add(assetHandle.Result.GetComponentInChildren<MeshFilter>().sharedMesh);
            }
        }

        public static Mesh GetItemMesh(int id)
            => id < _uniqueItemMeshes.Count ? _uniqueItemMeshes[id] : _colorCodedItemMeshes[id % _colorCodedItemMeshes.Count];

        private IEnumerator Start()
        {
            if (_initializationStarted) yield break;
            _initializationStarted = true;

            _uniqueItemMeshes = new List<Mesh>();
            _colorCodedItemMeshes = new List<Mesh>();

            yield return Load(UniqueItemsResourceLocation, _uniqueItemMeshes);
            yield return Load(ColorCodedItemsResourceLocation, _colorCodedItemMeshes);

            Initialized = true;
        }

        private void OnDestroy()
        {
            //TODO add unload
        }
        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        public static void Reset()
        {
            _initializationStarted = false;
            Initialized = false;
        }
    }
}