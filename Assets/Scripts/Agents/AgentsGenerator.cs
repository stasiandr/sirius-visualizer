using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Map;
using Map.Path;
using Misc;
using Miscellaneous;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Agents
{
    public class AgentsGenerator : MonoBehaviour
    {
        public AddressableLoader resourceLoader;
        public AssetReference agent;

        public Path basePath;
        
        public bool initializeOnStart = true;
        public TextAsset json;
        
        private IEnumerator Start()
        {
            yield return null;
            if (!initializeOnStart)
                yield break;
            
            while (!resourceLoader.Initialized || !MapGenerator.Initialized)
                yield return null;

            yield return GenerateAgents(new OrdersDataImporter(json.text));
        }

        public static Path BasePath;
        private static OrdersDataImporter _ordersData;
        public static OrdersDataImporter.OrderData? NextOrder =>
            _ordersData.Orders.Count != OrderProgress ? _ordersData.Orders[OrderProgress++] : (OrdersDataImporter.OrderData?) null;
        public static int OrderProgress;
        
        public IEnumerator GenerateAgents(OrdersDataImporter ordersDataImporter)
        {
            _ordersData = ordersDataImporter;
            OrderProgress = 0;
            BasePath = basePath;
            
            var start = MapGenerator.MapData.StartPosition;

            for (int i = 0; i < _ordersData.AgentsCount; i++)
            {
                var handle = agent.InstantiateAsync(start.ToWorldPosition(.5f), Quaternion.identity, transform);
                yield return handle;
 
                yield return new WaitForSeconds(GameSettings.SpawnAgentDelay);
            }
        }
    }
}