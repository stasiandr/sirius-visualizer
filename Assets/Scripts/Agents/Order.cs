using System.Collections.Generic;

namespace Agents
{
    public class Order
    {
        public readonly List<OrdersDataImporter.OrderItemsData> Orders;
        public int OrderProgress;

        public Order(List<OrdersDataImporter.OrderItemsData> orders)
        {
            Orders = orders;
            OrderProgress = 0;
        }

        public OrdersDataImporter.OrderItemsData Next() => Orders[OrderProgress++];

        public bool IsComplete => OrderProgress == Orders.Count;

    }
}