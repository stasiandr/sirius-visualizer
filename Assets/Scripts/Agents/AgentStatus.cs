namespace Agents
{
    public enum AgentStatus
    {
        Awaiting,
        Moving,
        PickingItem,
        PlacingItem
    }
}