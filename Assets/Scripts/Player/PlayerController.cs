using System;
using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        public static Transform FollowTransform;
        public new Transform camera;
        
        public float normalSpeed;
        public float fastSpeed;

        public float moveTime;
        public float rotationAmount;
        public Vector3 zoomAmount;

        private Vector3 newPosition;
        private Quaternion newRotation;
        private Vector3 newZoom;

        private Vector3 dragStartPosition;
        private Vector3 dragPosition;
        
        private void Start()
        {
            newPosition = transform.position;
            newRotation = transform.rotation;
            newZoom = camera.localPosition;
        }

        private void LateUpdate()
        {
            if (FollowTransform != null)
            {
                newPosition  = FollowTransform.position;
            }
            
            HandleMouseMovement();
            HandleMouseInput();

            UpdateTransform();

            if (Input.GetKeyDown(KeyCode.Escape))
                UnfollowTarget();
        }

        private void UpdateTransform()
        {
            transform.position = Vector3.Lerp(transform.position, newPosition, moveTime * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, moveTime * Time.deltaTime);
            camera.localPosition = Vector3.Lerp(camera.localPosition, newZoom, moveTime * Time.deltaTime);
        }

        public static void UnfollowTarget()
        {
            FollowTransform = null;
        }

        private void HandleMouseInput()
        {
            newZoom += zoomAmount * -Input.mouseScrollDelta.y;
            
            if (FollowTransform != null)
                return;

            if (Input.GetMouseButtonDown(0))
            {
                Plane plane = new Plane(Vector3.up, Vector3.zero);
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (plane.Raycast(ray, out var entry))
                {
                    dragStartPosition = ray.GetPoint(entry);
                }
            }

            if (Input.GetMouseButton(0))
            {
                Plane plane = new Plane(Vector3.up, Vector3.zero);
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (plane.Raycast(ray, out var entry))
                {
                    dragPosition = ray.GetPoint(entry);

                    newPosition = transform.position + dragStartPosition - dragPosition;
                }
            }
        }


        private void HandleMouseMovement()
        {
            var moveSpeed = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)
                ? fastSpeed
                : normalSpeed;
            
            if (Input.GetKey(KeyCode.Q)) newRotation *= Quaternion.Euler(Vector3.up * rotationAmount);
            if (Input.GetKey(KeyCode.E)) newRotation *= Quaternion.Euler(Vector3.up * -rotationAmount);

            if (Input.GetKey(KeyCode.R)) newZoom += zoomAmount;
            if (Input.GetKey(KeyCode.F)) newZoom -= zoomAmount; 
            
            if (FollowTransform != null)
                return;

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) newPosition += transform.forward * moveSpeed;
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) newPosition -= transform.forward * moveSpeed;
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) newPosition += transform.right * moveSpeed;
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) newPosition -= transform.right * moveSpeed;
        }
    }
}