using System;
using System.Collections.Generic;
using System.Linq;
using Gameplay;
using Misc;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Extensions = Misc.Extensions;

namespace StaticData
{
    public readonly struct MapDataImporter
    {
        public readonly Vector2Int Dimensions;
        public readonly Vector2Int[] ShelvesPositions;

        public readonly Vector2Int StartPosition;

        public readonly Vector2Int[] EndPositions;

        public readonly EntitiesOnMap[,] Map;

        public MapDataImporter(Vector2Int dimensions, Vector2Int[] shelvesPositions, Vector2Int startPosition, Vector2Int[] endPositions, EntitiesOnMap[,] map)
        {
            Dimensions = dimensions;
            ShelvesPositions = shelvesPositions;
            StartPosition = startPosition;
            EndPositions = endPositions;
            Map = map;
        }

        public MapDataImporter(Texture2D mapTexture)
        {
            Dimensions = new Vector2Int(mapTexture.width, mapTexture.height);

            var shelves = new List<Vector2Int>();
            var ends = new List<Vector2Int>();
            Map = new EntitiesOnMap[Dimensions.x, Dimensions.y];
            StartPosition = -Vector2Int.one;

            // TODO convert to enumerator
            for (int x = 0; x < Dimensions.x; x++)
            {
                for (int y = 0; y < Dimensions.y; y++)
                {
                    Map[x, y] = ColorToEntityOnMap(mapTexture.GetPixel(x, y));
                    switch (Map[x, y])
                    {
                        case EntitiesOnMap.Free:
                            break;
                        case EntitiesOnMap.Shelf:
                            shelves.Add(new Vector2Int(x, y));
                            break;
                        case EntitiesOnMap.Start:
                            if (StartPosition != -Vector2Int.one)
                                throw new ArgumentException("More than one StartPoints is set");
                            StartPosition = new Vector2Int(x, y);
                            break;
                        case EntitiesOnMap.DropZone:
                            ends.Add(new Vector2Int(x, y));
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }

            ShelvesPositions = shelves.ToArray();
            EndPositions = ends.ToArray();

        }

        public MapDataImporter(string mapRequestText)
        {
            var json = JObject.Parse(mapRequestText)["map"];

            Dimensions = json["dimensions"].ToVector2Int();
            ShelvesPositions = json["shelves"].Select(Extensions.ToVector2Int).ToArray();
            EndPositions = json["dropZone"].Select(Extensions.ToVector2Int).ToArray();
            StartPosition = json["start"].ToVector2Int();

            Map = new EntitiesOnMap[Dimensions.x, Dimensions.y];
            Map.Fill(EntitiesOnMap.Free);

            foreach (var pos in ShelvesPositions) Map.Set(pos, EntitiesOnMap.Shelf);
            foreach (var dropZone in EndPositions) Map.Set(dropZone, EntitiesOnMap.DropZone);
            
            Map.Set(StartPosition, EntitiesOnMap.Start);
        }

        public bool[,] Filter(EntitiesOnMap entity)
        {
            bool[,] answer = new bool[Dimensions.x, Dimensions.y];
            
            foreach (var index in Map.EnumerateIndexes())
            {
                answer[index.x, index.y] = Map[index.x, index.y] == entity;
            }

            return answer;
        }

        private static EntitiesOnMap ColorToEntityOnMap(Color col)
        {
            if (col == Color.white)
                return EntitiesOnMap.Shelf;
            if (col == Color.red)
                return EntitiesOnMap.Start;
            if (col == Color.green)
                return EntitiesOnMap.DropZone;
            return EntitiesOnMap.Free;
        }
    }
}